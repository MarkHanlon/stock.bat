@echo off
title Stock
setlocal enabledelayedexpansion
color c
:top
echo Stock.bat by Rever.
echo Press any key to begin the game.
pause >nul
:begingame
cls 

  Set cash=50
  Set day=0
  SET /A stockprice=%RANDOM% * 10 / 32768 + 20
  Set %stockticker%=0

:loop
  set /A day = %day% + 1 
  cls
echo #################################
echo Day %day%            
echo Cash:%cash%           
echo Current Stock Owned:%stockticker%       
echo #################################
        
    


SET /A ran1=%RANDOM% * 10 / 32768 - 5
::if %ran1% == 1 goto :mcrash 
set /A stockprice = %stockprice% + %ran1%
if %stockprice% lss 0 goto :mcrash
echo %ran1% >> Randomiser Log
echo Change from yesterday: %ran1%
echo Current Price:%stockprice%
echo #################################
goto :skip1
:cryon
cls
echo #################################
echo Day %day%            
echo Cash:%cash%           
echo Current Stock Owned:%stockticker%       
echo #################################
echo Change from yesterday: %ran1%
echo Current Price:%stockprice%
echo #################################
:skip1
::echo P to pass, B to buy or S to sell. E to exit as a winner.
::set /p choice=
CHOICE /N /D c /T 2 /C:pbsec /M "Pick an option, (P)ass, (B)uy, (S)ell, OR (E)xit"%1
if ERRORLEVEL ==5 GOTO :instaskip
IF ERRORLEVEL ==4 GOTO :4
IF ERRORLEVEL ==3 GOTO :3
IF ERRORLEVEL ==2 GOTO :2
IF ERRORLEVEL ==1 GOTO :1
::if %choice%==p goto :1
::if %choice%==b goto :2
::if %choice%==s goto :3
::if %choice%==e goto :4
:instaskip
goto :loop
:1
echo Day %day%: Skipped Day >> PlayHistory.log
echo Skipping Day.
timeout 1 /nobreak >nul
goto :loop
:2
if %cash% gtr %stockprice% goto :sucb
goto :invb
:sucb 
echo day %day%: Executed Buy Order >> PlayHistory.log
echo Bought successfully!
set prevcash=%cash%
set /A cash = %cash% - %stockprice%
set /a stockticker= stockticker + 1
timeout 1 /nobreak >nul
goto :loop
:invb
echo Sorry, you dont have enough cash!
timeout 1 /nobreak >nul
goto :cryon
:3
if %stockticker% gtr 0 goto :sels
goto :invs
:sels
echo day %day%: Executed Sell Order >> PlayHistory.log
echo Sold Successfully!
set /a cash = %cash% + %stockprice%*%stockticker%
set /a stockticker=0
timeout 1 /nobreak >nul
goto :loop 
:invs
echo Sorry, no stocks to sell!
goto :loop
:mcrash
SET /A mcran=%RANDOM% * 2 / 32768 + 1
if %mcran%==1 goto :mccrash 
goto :cryon
:mccrash
echo day %day%: Market Crashed >> PlayHistory.log
echo Market Crashed!
echo You lost all your cash!
echo You were on day %day%.
set cash=0
goto :eog
pause
cls
goto :begingame 
:4
echo day %day%: Took Cash.
echo You had %cash% cash
echo You were on day %day%
echo Winner!
goto :eog
:eog
echo Adding your score to a list of scores...
echo %time% %date%, Cash: %cash% Day: %day% >> hsc.log
echo %hscl% 
pause
cls
goto :begingame
:timeendofgame
set /a cash = %cash% + %stockprice%*%stockticker%
set /a stockticker=0
echo You reached day 50.
echo You had %cash% cash,
echo You were on day 30
echo (1) Play again
echo (2) End game
set /p plyorl=
if %plyorl%=1 goto :begingame
if %plyorl%=2 exit
pause
exit
